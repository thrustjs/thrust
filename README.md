thrustJS v0.1.9
===============

thrustJS é um web container de execução/interpretação JavaScript, ou seja, é um Server-side JavaScript ou (SSJS).
Ele permite a escrita em JavaScript de códigos a serem interpretados pelo servidor Web, da mesma forma como ocorre com o PHP, por exemplo.

## What's new?

* v.0.1.9 - addSoftwareLibrary: uma *builtin function* que carrega dinamicamente JARs para o ambiente do thrustjs.
* v.0.1.8 - Liberado um REPL (ambiente iterativo) para o thrustjs.
* v.0.1.7 - Redefinição dos diretórios para priorizar e ordenar a carga dos módulos do thrustjs.
* v.0.1.5 - Load dinâmico de JARs a partir do WEB-INF\lib. Many bugs fixed.
* v.0.1.4 - Módulo de roteamento liberado.
* v.0.1.3 - Módulo de segurança liberado.
* v.0.1.2 - Módulo de acesso a base dados melhorado.
* v.0.1.1 - Carga dinâmica de módulos implementada.

## Tutorial

#### 1o Passo - Instalando o thrustjs
Criar um diretório com o nome da sua aplicação e copiar o thrustjs.<br>
A estrutura de diretório ficará mais ou menos assim:

```
+-- thrust
|   +-- common
|   +-- core
|       +-- .platform.crypt.js
|       +-- fs.js
|       +-- http.js
|   +-- priority
|       +-- database.js
|       +-- rotas.js
|   --- config.json
|   +-- doc
|       +-- fonts
|       +-- img
|       +-- scripts
|       +-- styles
+-- README.md
+-- thrust.jar
```

#### 2o Passo - Getting Started
Como construir o bom e velho "Hello World"?
Crie um arquivo dentro do diretório \thrust\app chamado *test.js*
Depois edite o arquivo *test.js* e crie uma função chamada *hello* e exporte utilizando a variável de módulo *export*.

```javascript
/**
 * Serviço que responde a URI _/test/hello_
 * @param {Object} param - objeto com os parâmetros da requisição.
 * @param {http.Request} request - wrapper da classe Request do Java.
 * @param {http.Response} response - wrapper da classe Response do Java.
 */
function hello(par, req, res) {
    res.write("Hello World!");
}

exports = {
    hello: hello
}
```

Agora basta chamar a URL http://localhost:8080/test/hello a partir do browser e será _renderizado_ 'Hello World!'



#### 3o Step
Under construction...


#### 4o Step
Under construction...


#### 5o Passo
Under construction...

#### Debugging
Inicialize o servidor com o parâmetro *agentlib* e suas opções conforme exemplo abaixo:

`java -agentlib:jdwp=server=y,suspend=y,transport=dt_socket,address=8888 -jar thrust.jar`

Digite a intrução de debug no local do código que deseja depurar:

`debugger;`

Inialize o NetBeans e siga as instruções contidas na URL <https://blogs.oracle.com/sundararajan/remote-debugging-of-nashorn-scripts-with-netbeans-ide-using-debugger-statements> e...


Divirta-se!


## License
Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the 'Software'), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED 'AS IS', WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

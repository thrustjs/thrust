var File = Java.type('java.io.File');
var Files = Java.type('java.nio.file.Files');
var Path = Java.type('java.nio.file.Path');
var BufferedReader = Java.type('java.io.BufferedReader');
var InputStreamReader = Java.type('java.io.InputStreamReader');


/**
 * Agrupa funcionalidades relativas a comunica&ccedil;&atilde;o http 
 * entre o browser e o servidor.
 * @namespace http
 */
var http = {

    endpoints: {
    },

    vroutes: {
        '/': function runningThrust(params, request, response) {
            response.setContentType('text/html; charset=utf-8')
            response.write('<H1>Thrust is running!!!</H1>')
        }
    },

    middlewares: [],

    mapEndPoint: function (virtual, realRoute) {
        http.endpoints[virtual] = realRoute
    },

    /**
     * Função que mapeia as rotas da aplicação.
     * @param {string} virtualRoute - *string* contendo a  nova rota (rota virtual) a ser mapeada
     * @param {string | function} realRoute - contém a rota real ou a função a ser executada para a rota mapeada. 
     * 
     * @example
     * http.addRoutes(<rota virtual>], <rota real | function>);
     */
    addRoute: function (virtualRoute, realRoute) {
        http.vroutes[virtualRoute] = realRoute
    },

    /**
     * Função que adciona funções (middlewares) a serem executada(s) a cada requisição http.
     * @param {function} middleware - função a ser executada para a cada requisição; deve retornar *true* ou *false*. 
     * 
     * @example
     * http.addRoutes(<rota virtual>], <rota real | function>);
     */
    addMiddleware: function (middleware) {
        middlewares.push(middleware)
    },

    /*mimes: ['text/html; charset=utf-8', 'text/xml; charset=utf-8', 'text/plain; charset=utf-8', 'text/javascript; charset=utf-8', 'application/json; charset=utf-8'],
     */
    mimes: ['text/html', 'text/xml', 'text/plain', 'text/javascript', 'application/javascript', 'application/json'],

    serializeParams: platf.serializeParams,

    /**
     * Executa o <i>parse</i> dos par&acirc;metros passados pelo client web
     * para o servidor.
     * 
     * @param {string}
     *            strParams A string contendo linha com os par&acirc;metros
     *            recebidos pelo servidor.
     * @returns {Object}
     * 
     * @ignore
     */
    parseParams: platf.parseParams,

    /**
     * Wrapper para Java HttpResponse.
     * @class
     * @memberof http
     * @param {HttpResponse} javaResponse
     */
    Response: function (javaResponse) {
        this.jres = javaResponse
        this.out = []
        this.headers = {}
        this.contentLength = null
        this['content-type'] = 'text/html; charset=utf-8'
    },

    /**
     * Wrapper para Java HttpRequest.
     * @class
     * @memberof http
     * @param {HttpRequest} javaRequest
     */
    Request: function (javaRequest) {
        this.jreq = javaRequest
    },

}; /* End namespace http */
http.vroutes['/thrust'] = http.vroutes['/']


http.old_uploadFile = function (path, fileName, request) {
    var contentType = request.contentType;

    /* Excluido para comtemplar envio de imagens via AJAX */
    /*if ((contentType != null) && (contentType.indexOf('multipart/form-data') >= 0)) {*/
    var fin = new java.io.DataInputStream(request.jreq.getInputStream());
    var formDataLength = request.jreq.getContentLength();
    var dataBytes = java.lang.reflect.Array.newInstance(java.lang.Byte.TYPE, formDataLength);
    var byteRead = 0;
    var totalBytesRead = 0;

    while (totalBytesRead < formDataLength) {
        byteRead = fin.read(dataBytes, totalBytesRead, formDataLength);
        totalBytesRead += byteRead;
    }

    var content = dataBytes;

    fs.saveToFile(path + "\\" + fileName, content);
}

http.Request.prototype = {

    /**
     * @property {String} queryString
     */
    get queryString() {
        var contentType = this.jreq.getContentType() || "";
        var qs = '';

        if (contentType.indexOf("multipart/form-data") == -1) {
            qs = this.jreq.getReader().readLine();
            qs = (qs === null || qs === '') ? this.jreq.getQueryString() : qs;
            qs = (qs === null) ? '' : URLDecoder.decode(qs, 'UTF-8');
        }

        return qs;
    },

    /**
     * @property {String} contentType
     */
    get contentType() {
        return this.jreq.getContentType() || "";
    },

    get method() {
        return this.jreq.getMethod().toUpperCase();
    },

    get pathInfo() {
        return this.jreq.getPathInfo();
    },

    get requestURI() {
        return this.jreq.getRequestURI();
    },

    get session() {
        return this.jreq.getSession();
    },

    get scheme() {
        return this.jreq.getScheme();
    },

    get host() {
        return this.jreq.getServerName();
    },

    get port() {
        return this.jreq.getServerPort();
    },

    get contextPath() {
        return this.jreq.getContextPath();
    },

    get rest() {
        var uri = this.jreq.getRequestURI();
        return uri.replace(this.contextPath, '');
    },

    getHeader: function (name) {
        return this.jreq.getHeader(name);
    },
    /**
     * @function {getParts} - Retorna uma coleção de '*javax.servlet.http.Parts*', que por definição
     *  *"represents a part as uploaded to the server as part of a multipart/form-data 
     * request body. The part may represent either an uploaded file or form data."*
     * @return {type} {description}
     */
    getParts: function () {
        if (this.contentType.indexOf("multipart/form-data") == -1)
            return new java.util.ArrayList();

        return this.jreq.getParts();
    }
};

http.Response.prototype = {
    clean: function () {
        this.out = [];
        this.headers = {};
        this.contentLength = null;
        this['content-type'] = 'text/html';
        this.charset = 'utf-8';
    },
    /**
     * Retorna o '*content-type*' do objeto Response
     * @function {getContentType} 
     * @return {String} retorno o tipo do *'contet-type'* (ex: application/json, text/html etc)
     */
    getContentType: function () {
        return this['content-type'];
    },
    /**
     * Modifica o *content-type* (tipo do conteúdo) da resposta da requisição.
     * @param {String} type - tipo do conteúdo (ex: 'text/html', 'application/json' etc).
     */
    setContentType: function (type) {
        this['content-type'] = type;
    },
    getCharacterEncoding: function () {
        return this.charset;
    },
    setCharacterEncoding: function (charset) {
        this.charset = charset;
    },
    /**
     * Escreve em formato *texto* o conteúdo passado no parâmetro *content* como resposta 
     * a requisição. Modifica o valor do *content-type* para *'text/html'*.
     * @param {Object} data - dado a ser enviado para o cliente.
     * @param {Number} statusCode - (opcional) status de retorno do request htttp.
     * @param {Object} headers - (opcional) configurações a serem definidas no header http.
     */
    write: function (content) {
        this.out.push(content);
        return this;
    },
    setOut: function (content) {
        this.out = [content];
        return this;
    },
    toBytes: function () {
        return new java.lang.String(this.out).getBytes();
    },
    toJson: function () {
        return (typeof (this.out[0]) == 'object') ? JSON.stringify(this.out[0]) : this.out.join('');
    },
    toString: function () {
        return this.out.join('');
    },
    addHeader: function (name, value) {
        this.headers[name] = value;
    },
    getStatus: function () {
        return this.status;
    },
    /**
     * Modifica o *status code* (código de retorno) da resposta da requisição.
     * @param {Number} statusCode - código de retorno a ser configurado.
     */
    setStatus: function (statusCode) {
        this.status = statusCode;
    },
    /**
     * Modifica o tamanho do conteúdo da resposta da requisição.
     * @param {Number} contentLength - tamanho da resposta (conteúdo) a ser enviada para o cliente.
     */
    setContentLength: function (contentLength) {
        this.contentLength = contentLength;
    },
    getContentLength: function () {
        return this.contentLength;
    },
    /**
     * Escreve em formato *JSON* o objeto passado no parâmetro *data* como resposta 
     * a requisição. Modifica o valor do *content-type* para *'application/json'*.
     * @param {Object} data - dado a ser enviado para o cliente.
     * @param {Number} statusCode - (opcional) status de retorno do request htttp.
     * @param {Object} headers - (opcional) configurações a serem definidas no header http.
     */
    json: function (data, statusCode, headers) {
        var ths = this;
        this['content-type'] = 'application/json';
        this.status = statusCode || 200;

        for (var opt in (headers || {})) {
            ths.headers[opt] = headers[opt];
        };

        this.out[0] = (typeof (data) == 'object') ? JSON.stringify(data) : data;
    },

    /**
     * Objeto que encapsula os métodos de retornos quando ocorre um erro na requisição http.
     * @ignore
     */
    error: {
        /**
         * Escreve em formato *JSON* uma mensagem de erro como resposta a requisição no
         * formato {message: *message*, status: *statusCode*}. Modifica o valor 
         * do *content-type* para *'application/json'*. 
         * @alias error.json
         * @memberof! http.Response#
         * @instance error.json
         * @param {String} message - mensagem de erro a ser enviada no retorno da chamada do browser.
         * @param {Number} statusCode - (opcional) status de retorno do request htttp.
         * @param {Object} headers - (opcional) configurações a serem definidas no header http.
         */
        json: function (message, statusCode, headers) {
            var ths = this;
            this['content-type'] = 'application/json';
            this.status = statusCode || 200;

            for (var opt in (headers || {})) {
                ths.headers[opt] = headers[opt];
            };

            this.out[0] = JSON.stringify({
                status: ths.status,
                message: message
            });
        }
    }
}

exports = http
